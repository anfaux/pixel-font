// Includes
"use strict";
require("dotenv").config({ path: require("find-config")(".env") });
const bitmapManipulation = require("bitmap-manipulation");

const bitmap = new bitmapManipulation.BMPBitmap(12, 12);
bitmap.drawFilledRect(0, 0, 12, 12, null, 0xff, null);
//img.changeColorDepth(4);

const outputPath = `TEST.bmp`;
bitmap.save(outputPath);
