const bitmapManipulation = require("bitmap-manipulation");

module.exports = {
  emptyBitmap: (size, bgColor = 0xff) => {
    const bitmap = new bitmapManipulation.BMPBitmap(size.width, size.height);
    bitmap.drawFilledRect(0, 0, size.width, size.height, null, bgColor, null);
    return bitmap;
  },
};
