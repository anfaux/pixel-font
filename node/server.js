// Includes
"use strict";
require("dotenv").config({ path: require("find-config")(".env") });
const fs = require("fs");
const path = require("path");
const cors = require("cors");
const ffmpegPath = require("@ffmpeg-installer/ffmpeg").path;
const ffmpeg = require("fluent-ffmpeg");
const express = require("express");
const nocache = require("nocache");
const { v4: uuidv4 } = require("uuid");
const app = express();
const bitmapHelper = require("./modules/bitmapHelper.js");
const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));
let font_5x7 = require("./fonts/font-5x7.json");
let font_hell = require("./fonts/hell-72-gl.json");

// Config
const fonts = { fiveby: font_5x7, hell: font_hell };
const outPath = path.join(__dirname, "TMP");
let config = {
  keepArtifacts:
    process.env.KEEP_ARTIFACTS?.toLowerCase().trim() === "true" ? true : false,
  port: process.env.PORT || 9999,
  outPath,
};
if (!config.keepArtifacts) fs.rmSync(outPath, { recursive: true, force: true });
if (!fs.existsSync(outPath)) {
  fs.mkdir(outPath, (err) => {
    if (err) console.error(err);
  });
}
// Init
ffmpeg.setFfmpegPath(ffmpegPath);
app.use(cors());
app.use(nocache());
app.use(express.json());
app.engine("html", require("ejs").renderFile);
app.listen(config.port);
app.use(express.static("public"));

console.log("--------------------------------");
console.log(`| Hello PixelFont!`);
console.log("--------------------------------");
console.log(`| KEEP ARTIFACTS: ${config.keepArtifacts}`);
console.log("--------------------------------");

// Routing
app.get("/", (req, res) => {
  if (!req.query.m) {
    if (process.env.HELP_URL) {
      res.redirect(301, process.env.HELP_URL);
    } else {
      res.sendStatus(400);
    }
    return;
  }
  const returnType = req.query.rt || "mp4";
  const text = req.query.m || "";
  const f = req.query.f || "fiveby";
  const font = fonts[f] ? fonts[f] : fonts.fiveby;
  const letterSpace = parseInt(req.query.ls, 10) || 1;
  const invert = req.query.i === "true" ? true : false;
  const scale = parseInt(req.query.s, 10) || 1;

  config = { ...config, outPath: `${outPath}/${uuidv4()}` };
  const _removeArtifacts = () => {
    if (!config.keepArtifacts)
      fs.rmSync(config.outPath, { recursive: true, force: true });
  };
  if (!fs.existsSync(config.outPath)) {
    fs.mkdir(config.outPath, (err) => {
      if (err) console.error(err);
    });
  }

  let message = [];
  let totalSpaces = 0;
  let fullSpace = Array.apply(null, Array(font.size.height)).map((x, i) =>
    Array.apply(null, Array(font.size.width)).map((x) => 0)
  );
  text.split("").forEach((char, idx) => {
    if (char === " ") {
      message.push(fullSpace);
      totalSpaces++;
    } else {
      message.push(font[char] ? font[char] : font["?"]);
    }
  });

  const messageWidth = message.length * (font.size.width + letterSpace);
  const bitmapSize = {
    width: (messageWidth - letterSpace) * scale,
    height: font.size.height * scale,
  };
  const bitmap = bitmapHelper.emptyBitmap(bitmapSize, invert ? 0xff : 0x00);

  let offset = 0;
  message.forEach((char, idx) => {
    char.forEach((row, y) => {
      row.forEach((pixel, x) => {
        bitmap.drawFilledRect(
          x + offset * scale,
          y * scale,
          1 * scale,
          1 * scale,
          null,
          `${pixel === 1 ? (invert ? 0x00 : 0xff) : invert ? 0xff : 0x00}`,
          null
        );
      });
    });
    offset += char[0].length + letterSpace;
  });

  const outputPath = `${config.outPath}/data.bmp`;
  bitmap.changeColorDepth(4); // Fixes a bug with this lib where small pixel images (<10px) glitch
  bitmap.save(outputPath);
  res.sendFile(outputPath, _removeArtifacts);
});
app.get("/favicon.ico", (req, res) => res.status(204));
app.get("*", (req, res) => {
  res.sendStatus(403);
});
