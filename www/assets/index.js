mouseIsDown = false;
font = localStorage.getItem("font");
defaultFont = {
  meta: {
    size: { width: 5, height: 7 },
    glyphs:
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()",
  },
};
font = font ? JSON.parse(font) : defaultFont;

const update = (id) => {
  const glyph = document.getElementById(id);
  const rows = glyph.getElementsByClassName("row");
  const newGlyph = [];
  Array.from(rows).forEach((row) => {
    newGlyphRow = [];
    pixelsInRow = row.getElementsByClassName("pixel");
    Array.from(pixelsInRow).forEach((pixel) => {
      newGlyphRow.push(pixel.classList.contains("selected") ? 1 : 0);
    });
    newGlyph.push(newGlyphRow);
  });
  localStorage.setItem("font", JSON.stringify({ ...font, [id]: newGlyph }));
  font = localStorage.getItem("font");
  font = font ? JSON.parse(font) : {};
};

const onGlyphDimensionChange = () => {
  const width = parseInt(document.getElementById("glyphW").value.trim(), 10);
  const height = parseInt(document.getElementById("glyphH").value.trim(), 10);
  font.meta.size = {
    width: width <= 0 || isNaN(width) ? 1 : width,
    height: height <= 0 || isNaN(height) ? 1 : height,
  };
  document.getElementById("glyphW").value = font.meta.size.width;
  document.getElementById("glyphH").value = font.meta.size.height;
  localStorage.setItem("font", JSON.stringify({ ...font }));
  setupPage();
};

const onGlyphListChange = () => {
  const glyphList = document.getElementById("glyphList").value.trim();
  font.meta.glyphs = glyphList;
  localStorage.setItem("font", JSON.stringify({ ...font, glyphs: glyphList }));
  setupPage();
};

const load = () => {
  const glyphList = document.getElementById("glyphList");
  glyphList.value = font.meta.glyphs;
  document.getElementById("glyphW").value = font.meta.size.width;
  document.getElementById("glyphH").value = font.meta.size.height;
  const glyphs = document.getElementsByClassName("glyph");
  Array.from(glyphs).forEach((glyph) => {
    const rows = glyph.getElementsByClassName("row");
    Array.from(rows).forEach((row, rIdx) => {
      pixelsInRow = row.getElementsByClassName("pixel");
      Array.from(pixelsInRow).forEach((pixel, cIdx) => {
        if (font[glyph.id] && font[glyph.id][rIdx][cIdx] === 1) {
          pixel.classList.add("selected");
        }
      });
    });
  });
};

const addGlyph = (id) => {
  const glyph = document.createElement("div");
  glyph.className = "glyph";
  glyph.id = id;
  glyph.innerHTML = id;
  for (let idx = 0; idx < font.meta.size.height; idx++) {
    const row = document.createElement("div");
    row.className = "row";
    for (let idx2 = 0; idx2 < font.meta.size.width; idx2++) {
      const pixel = document.createElement("div");
      pixel.className = "pixel";
      row.appendChild(pixel);
    }
    glyph.appendChild(row);
  }
  document.getElementById("content").appendChild(glyph);
};

const setupPage = () => {
  document.getElementById("content").innerHTML = "";
  const onPixelClick = (e) => {
    if (window.event) e = e.target;
    if (e.className && e.className.indexOf("pixel") != -1) {
      const id = e.parentElement.parentElement.id;
      if (e.className.indexOf("selected") != -1) {
        e.classList.remove("selected");
      } else {
        e.classList.add("selected");
      }
      update(id);
    }
  };

  document.getElementById("content").onclick = onPixelClick;
  document.getElementById("content").onmousedown = () => (mouseIsDown = true);
  document.getElementById("content").onmouseup = () => (mouseIsDown = false);
  document.getElementById("content").onclick = onPixelClick;

  document.getElementById("content").onmouseover = (e) => {
    if (mouseIsDown) {
      onPixelClick(e);
    }
  };
  console.log(`Name: ${font.meta.name}`);
  console.log(`Desc: ${font.meta.description}`);
  font.meta.glyphs.split("").forEach((glyph) => addGlyph(glyph));
  load();
};

window.onload = () => {
  setupPage();
};

const onClearAll = () => {
  localStorage.setItem("font", JSON.stringify(defaultFont));
  font = localStorage.getItem("font");
  font = font ? JSON.parse(font) : {};
  setupPage();
};

const onExportJson = () => {
  const filename = "font.json";
  const str = JSON.stringify(font);
  let element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(str)
  );
  element.setAttribute("download", filename);
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
};

onLoadFont = (fontUrl) => {
  fetch(fontUrl)
    .then((response) => response.json())
    .then((data) => {
      console.log(`\nLoading ${fontUrl}...`);
      localStorage.setItem("font", JSON.stringify(data));
      font = localStorage.getItem("font");
      font = font ? JSON.parse(font) : {};
      setupPage();
    })
    .catch((error) => console.error(error));
};
