# [PixelFont Editor](https://pixelfont.foxriot.com/)

Vanilla HTML and CSS, hosted via Gitlab pages (source in `www` dir)

# PixelFont API

API for producing textures that contain text written in matrix font. Outputs as BMP, PNG or MP4. Uses the fonts produced by the editor.

| Variable |                                            |
| -------- | :----------------------------------------- |
| ?m=      | Message. Required.                         |
| ?rt=     | Return Type. Optional. [`png`,`bmp`,`mp4`] |
| ?f=      | Font. Optional [`hell`,`fiveby`]           |
| ?i=      | Inverse colors. Optional.                  |
| ?s=      | Scale. Optional.                           |

## Config

Use environment variables to establish default behavior. None of the options are required.
| Variable | |
| --------- | :--------------------------------------------------------------------------- |
| KEEP_ARTIFACTS | For debugging. If "true", api will not clean up after a request, leaving the BMP and MP4 files in a temp |
| HELP_URL | Full URL to a help page (returned if you call API without required param). Default 400. |

# Notes

[Hellschreiber 72 Font Info](https://www.nonstopsystems.com/radio/hellschreiber-fonts.htm)
